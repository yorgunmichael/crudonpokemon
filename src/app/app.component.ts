import {Component} from '@angular/core';

/*
Le template représente la vue de l'utilisateur
* */
@Component({
    selector: 'app-root',
    templateUrl: 'app.component.html'
})

/*
Ici j'affiche la liste des pokémons chargé à partir du fichier mock-pokemon-list.
Ensuite je passe en paramètre l'id d'un pokemon
* */
export class AppComponent {}





