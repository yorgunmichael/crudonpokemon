import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {PageNotFoundComponent} from "./page-not-found/page-not-found.component";

const routes: Routes = [

    {path: '', redirectTo: 'pokemons', pathMatch: 'full'},
    {path: '**', component: PageNotFoundComponent} // pour intercepter toute les routes autres que ceux de mon application
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
